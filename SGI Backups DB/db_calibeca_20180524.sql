﻿/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     24/5/2018 15:02:01                           */
/*==============================================================*/



/*==============================================================*/
/* Table: cat_becario                                           */
/*==============================================================*/
create table cat_becario (
   i_becario            SERIAL               not null,
   i_monto              INT4                 null,
   vc_cod_carr          VARCHAR(7)           null,
   vc_carnet            VARCHAR(7)           null,
   vc_nombre            VARCHAR(100)         null,
   vc_sexo              VARCHAR(1)           null,
   constraint PK_CAT_BECARIO primary key (i_becario)
);

comment on column cat_becario.i_becario is
'Identificador de llave primaria de la tabla';

comment on column cat_becario.vc_cod_carr is
'Identificador del codigo de la carrera';

comment on column cat_becario.vc_carnet is
'Carnet del estudiante';

comment on column cat_becario.vc_nombre is
'Nombre completo del becario';

/*==============================================================*/
/* Index: cat_becario_pk                                        */
/*==============================================================*/
create unique index cat_becario_pk on cat_becario (
i_becario
);

/*==============================================================*/
/* Index: relationship_23_fk                                    */
/*==============================================================*/
create  index relationship_23_fk on cat_becario (
vc_carnet
);

/*==============================================================*/
/* Index: relationship_15_fk                                    */
/*==============================================================*/
create  index relationship_15_fk on cat_becario (
vc_cod_carr
);

/*==============================================================*/
/* Index: fk_monto_becario_fk                                   */
/*==============================================================*/
create  index fk_monto_becario_fk on cat_becario (
i_monto
);

/*==============================================================*/
/* Table: cat_carrera                                           */
/*==============================================================*/
create table cat_carrera (
   vc_cod_carr          VARCHAR(7)           not null,
   vc_cod_fac           VARCHAR(2)           null,
   vc_nom_carr          VARCHAR(120)         null,
   constraint PK_CAT_CARRERA primary key (vc_cod_carr)
);

comment on column cat_carrera.vc_cod_carr is
'Identificador de llave primaria de la tabla';

comment on column cat_carrera.vc_cod_fac is
'Identificador del codigo de la facultad';

comment on column cat_carrera.vc_nom_carr is
'Nombre de la carrera';

/*==============================================================*/
/* Index: cat_carrera_pk                                        */
/*==============================================================*/
create unique index cat_carrera_pk on cat_carrera (
vc_cod_carr
);

/*==============================================================*/
/* Index: relationship_12_fk                                    */
/*==============================================================*/
create  index relationship_12_fk on cat_carrera (
vc_cod_fac
);

/*==============================================================*/
/* Table: cat_correo                                            */
/*==============================================================*/
create table cat_correo (
   i_correo             SERIAL               not null,
   vc_correo            VARCHAR(75)          null,
   vc_carnet            VARCHAR(7)           null,
   constraint PK_CAT_CORREO primary key (i_correo)
);

comment on column cat_correo.i_correo is
'Identificador de llave primaria de la tabla';

comment on column cat_correo.vc_correo is
'Correo electronico del becario';

comment on column cat_correo.vc_carnet is
'Carnet del estudiante';

/*==============================================================*/
/* Index: cat_correo_pk                                         */
/*==============================================================*/
create unique index cat_correo_pk on cat_correo (
i_correo
);

/*==============================================================*/
/* Index: relationship_21_fk                                    */
/*==============================================================*/
create  index relationship_21_fk on cat_correo (
vc_carnet
);

/*==============================================================*/
/* Table: cat_domicilio                                         */
/*==============================================================*/
create table cat_domicilio (
   i_domicilio          SERIAL               not null,
   vc_carnet            VARCHAR(7)           null,
   vc_telefono          VARCHAR(10)          null,
   constraint PK_CAT_DOMICILIO primary key (i_domicilio)
);

comment on column cat_domicilio.i_domicilio is
'Identificador de llave primaria de la tabla';

comment on column cat_domicilio.vc_carnet is
'Carnet del estudiante';

comment on column cat_domicilio.vc_telefono is
'Telefono del estudiante';

/*==============================================================*/
/* Index: cat_domicilio_pk                                      */
/*==============================================================*/
create unique index cat_domicilio_pk on cat_domicilio (
i_domicilio
);

/*==============================================================*/
/* Index: relationship_22_fk                                    */
/*==============================================================*/
create  index relationship_22_fk on cat_domicilio (
vc_carnet
);

/*==============================================================*/
/* Table: cat_estudiante                                        */
/*==============================================================*/
create table cat_estudiante (
   vc_carnet            VARCHAR(7)           not null,
   vc_nombre            VARCHAR(50)          null,
   vc_apellido          VARCHAR(50)          null,
   vc_cod_carr          VARCHAR(7)           null,
   vc_cod_facultad      VARCHAR(2)           null,
   constraint PK_CAT_ESTUDIANTE primary key (vc_carnet)
);

comment on column cat_estudiante.vc_carnet is
'Identificador de llave primaria de la tabla';

comment on column cat_estudiante.vc_nombre is
'Nombres del estudiante';

comment on column cat_estudiante.vc_apellido is
'Apellidos del estudiante';

comment on column cat_estudiante.vc_cod_carr is
'Identificador del codigo de la carrera';

comment on column cat_estudiante.vc_cod_facultad is
'Identificador del codigo de la facultad';

/*==============================================================*/
/* Index: cat_estudiante_pk                                     */
/*==============================================================*/
create unique index cat_estudiante_pk on cat_estudiante (
vc_carnet
);

/*==============================================================*/
/* Index: relationship_13_fk                                    */
/*==============================================================*/
create  index relationship_13_fk on cat_estudiante (
vc_cod_carr
);

/*==============================================================*/
/* Index: relationship_14_fk                                    */
/*==============================================================*/
create  index relationship_14_fk on cat_estudiante (
vc_cod_facultad
);

/*==============================================================*/
/* Table: cat_facultad                                          */
/*==============================================================*/
create table cat_facultad (
   vc_cod_facultad      VARCHAR(2)           not null,
   vc_nombre_facultad   VARCHAR(50)          null,
   constraint PK_CAT_FACULTAD primary key (vc_cod_facultad)
);

comment on column cat_facultad.vc_cod_facultad is
'Identificador de llave primaria de la tabla';

comment on column cat_facultad.vc_nombre_facultad is
'Nombre de la facultad';

/*==============================================================*/
/* Index: cat_facultad_pk                                       */
/*==============================================================*/
create unique index cat_facultad_pk on cat_facultad (
vc_cod_facultad
);

/*==============================================================*/
/* Table: cat_institucion                                       */
/*==============================================================*/
create table cat_institucion (
   i_institucion        SERIAL               not null,
   i_pais               INT4                 null,
   i_servidor           INT4                 null,
   vc_institucion       character varying(150) null,
   vc_razon_social      character varying(254) null,
   vc_direccion         character varying(150) null,
   vc_correo            character varying(75) null,
   b_eliminado          boolean              null,
   constraint PK_CAT_INSTITUCION primary key (i_institucion)
);

comment on table cat_institucion is
'Entidad que registra los datos generales de la empresa configurada para la aplicaciÃ³n';

comment on column cat_institucion.i_institucion is
'Identificador unico o llave primaria para la institucion';

comment on column cat_institucion.i_pais is
'Identificador unico o llave primaria del pais';

comment on column cat_institucion.i_servidor is
'Identificador del servidor de jasperserver';

comment on column cat_institucion.vc_institucion is
'Nombre corto de la institucion';

comment on column cat_institucion.vc_razon_social is
'Razon social o nombre completo de la institucion';

comment on column cat_institucion.vc_direccion is
'Direccion completa de la ubicacion exacta';

comment on column cat_institucion.vc_correo is
'Direccion de correo electronico';

comment on column cat_institucion.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: cat_institucion_pk                                    */
/*==============================================================*/
create unique index cat_institucion_pk on cat_institucion (
i_institucion
);

/*==============================================================*/
/* Index: relationship_24_fk                                    */
/*==============================================================*/
create  index relationship_24_fk on cat_institucion (
i_pais
);

/*==============================================================*/
/* Index: relationship_25_fk                                    */
/*==============================================================*/
create  index relationship_25_fk on cat_institucion (
i_servidor
);

/*==============================================================*/
/* Table: cat_modulo                                            */
/*==============================================================*/
create table cat_modulo (
   vc_modulo            character varying(16) not null,
   vc_descripcion       character varying(2000) not null,
   vc_usuario_crea      character varying(150) null,
   d_crea               DATE                 null,
   vc_usuario_modif     character varying(150) null,
   d_modifica           DATE                 null,
   b_eliminado          boolean              null,
   constraint PK_CAT_MODULO primary key (vc_modulo)
);

comment on table cat_modulo is
'Entidad que registra las aplicaciones, mÃ³dulos o componentes del sistema';

comment on column cat_modulo.vc_modulo is
'Identificador unico del modulo o componente del sistema';

comment on column cat_modulo.vc_descripcion is
'Detalles importantes a destacar';

comment on column cat_modulo.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column cat_modulo.d_crea is
'Fecha-hora de creaciÃ³n del registro';

comment on column cat_modulo.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column cat_modulo.d_modifica is
'Fecha-hora de modificaciÃ³n del registro';

comment on column cat_modulo.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: cat_modulo_pk                                         */
/*==============================================================*/
create unique index cat_modulo_pk on cat_modulo (
vc_modulo
);

/*==============================================================*/
/* Table: cat_monto                                             */
/*==============================================================*/
create table cat_monto (
   i_monto              SERIAL               not null,
   dc_monto             DECIMAL(18,2)        not null,
   constraint PK_CAT_MONTO primary key (i_monto)
);

/*==============================================================*/
/* Index: cat_monto_pk                                          */
/*==============================================================*/
create unique index cat_monto_pk on cat_monto (
i_monto
);

/*==============================================================*/
/* Table: cat_recurso                                           */
/*==============================================================*/
create table cat_recurso (
   i_recurso            SERIAL   not null,
   cat_i_recurso        INT4                 null,
   vc_tipo_recurso      character varying(16) null,
   vc_modulo            character varying(16) null,
   vc_usuario_crea      character varying(150) not null,
   vc_usuario_modif     character varying(150) null,
   vc_descripcion       character varying(254) not null,
   i_posicion           integer              not null,
   vc_icono             character varying(64) null,
   b_habilitado         integer              null,
   vc_url               character varying    null,
   i_detalle            integer              null,
   d_crea               DATE                 not null,
   dt_modifica          DATE                 null,
   vc_medio             character varying(3) null,
   b_recurso_externo    boolean              null,
   constraint PK_CAT_RECURSO primary key (i_recurso)
);

comment on table cat_recurso is
'Entidad que registra los recursos o opciones del sistema';

comment on column cat_recurso.i_recurso is
'Identificador unico del recurso u opcion del sistema';

comment on column cat_recurso.cat_i_recurso is
'Identificador unico del recurso u opcion del sistema';

comment on column cat_recurso.vc_tipo_recurso is
'Identificador unico del tipo de recurso del sistema';

comment on column cat_recurso.vc_modulo is
'Identificador unico de la aplicacion, modulo o componente del sistema';

comment on column cat_recurso.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column cat_recurso.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column cat_recurso.vc_descripcion is
'Descripcion o especificacion';

comment on column cat_recurso.i_posicion is
'Posicion u orden del recurso';

comment on column cat_recurso.vc_icono is
'Nombre del icono correspondiente al recurso';

comment on column cat_recurso.b_habilitado is
'Bandera que define si el recurso esta habilitado o no';

comment on column cat_recurso.vc_url is
'DirecciÃ³n o ruta de acceso al recurso del sistema';

comment on column cat_recurso.i_detalle is
'Bandera de ultimo nivel';

comment on column cat_recurso.d_crea is
'Fecha-hora de creacion del registro';

comment on column cat_recurso.dt_modifica is
'Fecha-hora de modificacion del registro';

comment on column cat_recurso.vc_medio is
'Identifica si es medio web o movil';

comment on column cat_recurso.b_recurso_externo is
'Identifica si es un recurso externo';

/*==============================================================*/
/* Index: cat_recurso_pk                                        */
/*==============================================================*/
create unique index cat_recurso_pk on cat_recurso (
i_recurso
);

/*==============================================================*/
/* Index: relationship_33_fk                                    */
/*==============================================================*/
create  index relationship_33_fk on cat_recurso (
cat_i_recurso
);

/*==============================================================*/
/* Index: relationship_34_fk                                    */
/*==============================================================*/
create  index relationship_34_fk on cat_recurso (
vc_tipo_recurso
);

/*==============================================================*/
/* Index: relationship_39_fk                                    */
/*==============================================================*/
create  index relationship_39_fk on cat_recurso (
vc_modulo
);

/*==============================================================*/
/* Table: cat_reg_materia                                       */
/*==============================================================*/
create table cat_reg_materia (
   i_reg_materia        SERIAL               not null,
   vc_carnet            VARCHAR(7)           null,
   d_nota_materia       DECIMAL(10)          null,
   vc_cod_mat           VARCHAR(6)           null,
   constraint PK_CAT_REG_MATERIA primary key (i_reg_materia)
);

comment on column cat_reg_materia.i_reg_materia is
'Identificador de llave primaria de la tabla';

comment on column cat_reg_materia.vc_carnet is
'Carnet del estudiante';

comment on column cat_reg_materia.d_nota_materia is
'Nota obtenida del sistema ADACAD';

/*==============================================================*/
/* Index: cat_reg_materia_pk                                    */
/*==============================================================*/
create unique index cat_reg_materia_pk on cat_reg_materia (
i_reg_materia
);

/*==============================================================*/
/* Index: relationship_20_fk                                    */
/*==============================================================*/
create  index relationship_20_fk on cat_reg_materia (
vc_carnet
);

/*==============================================================*/
/* Table: cat_servidor                                          */
/*==============================================================*/
create table cat_servidor (
   i_servidor           SERIAL               not null,
   vc_nombre            character varying(60) null,
   vc_direccion         character varying(50) null,
   vc_us_servidor       character varying(60) null,
   vc_password_servidor character varying(255) null,
   vc_cod_servidor      character varying(10) null,
   constraint PK_CAT_SERVIDOR primary key (i_servidor)
);

comment on column cat_servidor.i_servidor is
'Identificador de llave primaria de la tabla';

comment on column cat_servidor.vc_nombre is
'Nombre del servidor';

comment on column cat_servidor.vc_direccion is
'Dirrecion ip:puerto donde esta el servidor';

comment on column cat_servidor.vc_us_servidor is
'Usuario del servidor';

comment on column cat_servidor.vc_password_servidor is
'Clave del servidor';

comment on column cat_servidor.vc_cod_servidor is
'Abreviatura del servidor';

/*==============================================================*/
/* Index: cat_servidor_pk                                       */
/*==============================================================*/
create unique index cat_servidor_pk on cat_servidor (
i_servidor
);

/*==============================================================*/
/* Table: cat_tipo_recurso                                      */
/*==============================================================*/
create table cat_tipo_recurso (
   vc_tipo_recurso      character varying(16) not null,
   vc_usuario_crea      character varying(150) null,
   dt_fecha_crea        DATE                 null,
   vc_usuario_modif     character varying(150) null,
   dt_modifica          DATE                 null,
   b_eliminado          boolean              null,
   vc_descripcion       character varying(100) null,
   constraint PK_CAT_TIPO_RECURSO primary key (vc_tipo_recurso)
);

comment on table cat_tipo_recurso is
'Entidad que registra los tipos de recursos del sistema';

comment on column cat_tipo_recurso.vc_tipo_recurso is
'Identificador unico del tipo de recurso del sistema';

comment on column cat_tipo_recurso.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column cat_tipo_recurso.dt_fecha_crea is
'Fecha-hora de creacion del registro';

comment on column cat_tipo_recurso.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column cat_tipo_recurso.dt_modifica is
'Fecha-hora de modificaciÃ³n del registro';

comment on column cat_tipo_recurso.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

comment on column cat_tipo_recurso.vc_descripcion is
'descripcion breve del tipo de recurso';

/*==============================================================*/
/* Index: cat_tipo_recurso_pk                                   */
/*==============================================================*/
create unique index cat_tipo_recurso_pk on cat_tipo_recurso (
vc_tipo_recurso
);

/*==============================================================*/
/* Table: sys_cuf_acad                                          */
/*==============================================================*/
create table sys_cuf_acad (
   i_cuf                INT4                 not null,
   vc_carnet            VARCHAR(7)           null,
   vc_ciclo             VARCHAR(6)           null,
   d_promedio           NUMERIC(5,2)         null,
   constraint PK_SYS_CUF_ACAD primary key (i_cuf)
);

comment on column sys_cuf_acad.i_cuf is
'Identificador de llave primaria de la tabla';

comment on column sys_cuf_acad.vc_carnet is
'Carnet del estudiante';

comment on column sys_cuf_acad.vc_ciclo is
'Ciclo del sistema de notas';

comment on column sys_cuf_acad.d_promedio is
'Valor obtenido de las notas del estudiante';

/*==============================================================*/
/* Index: sys_cuf_acad_pk                                       */
/*==============================================================*/
create unique index sys_cuf_acad_pk on sys_cuf_acad (
i_cuf
);

/*==============================================================*/
/* Index: relationship_19_fk                                    */
/*==============================================================*/
create  index relationship_19_fk on sys_cuf_acad (
vc_carnet
);

/*==============================================================*/
/* Table: sys_estructura_org                                    */
/*==============================================================*/
create table sys_estructura_org (
   i_estructura_org     SERIAL               not null,
   sys_i_estructura_org INT4                 null,
   i_institucion        INT4                 null,
   vc_usuario_crea      character varying(150) null,
   dt_crea              DATE                 null,
   vc_usuario_modif     character varying(150) null,
   dt_modifica          DATE                 null,
   b_eliminado          boolean              null,
   constraint PK_SYS_ESTRUCTURA_ORG primary key (i_estructura_org)
);

comment on table sys_estructura_org is
'Entidad que registra la estructura organizativa de las instituciones';

comment on column sys_estructura_org.i_estructura_org is
'Identificador unico de la estructura organizativa';

comment on column sys_estructura_org.sys_i_estructura_org is
'Identificador unico de la estructura organizativa';

comment on column sys_estructura_org.i_institucion is
'Identificador unico o llave primaria para la institucion';

comment on column sys_estructura_org.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column sys_estructura_org.dt_crea is
'Fecha-hora de creaciÃ³n del registro';

comment on column sys_estructura_org.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column sys_estructura_org.dt_modifica is
'Fecha-hora de modificacion del registro';

comment on column sys_estructura_org.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: sys_estructura_org_pk                                 */
/*==============================================================*/
create unique index sys_estructura_org_pk on sys_estructura_org (
i_estructura_org
);

/*==============================================================*/
/* Index: relationship_29_fk                                    */
/*==============================================================*/
create  index relationship_29_fk on sys_estructura_org (
sys_i_estructura_org
);

/*==============================================================*/
/* Index: relationship_30_fk                                    */
/*==============================================================*/
create  index relationship_30_fk on sys_estructura_org (
i_institucion
);

/*==============================================================*/
/* Table: sys_grupo_usuario                                     */
/*==============================================================*/
create table sys_grupo_usuario (
   sys_vc_usuario       character varying(150) not null,
   sys_vc_grupo         character varying(150) not null,
   vc_grupo             character varying(75) not null,
   vc_usuarioc_rea      character varying(150) null,
   dt_crea              DATE                 null,
   vc_usuario_modif     character varying(150) null,
   dt_modifica          DATE                 null,
   constraint PK_SYS_GRUPO_USUARIO primary key (sys_vc_usuario, sys_vc_grupo)
);

comment on table sys_grupo_usuario is
'Entidad que registra los grupos de usuarios del sistema';

comment on column sys_grupo_usuario.sys_vc_usuario is
'Identificador unico del usuario del sistema';

comment on column sys_grupo_usuario.sys_vc_grupo is
'Identificador unico del usuario del sistema';

comment on column sys_grupo_usuario.vc_grupo is
'Identificador unico del grupo de usuarios';

comment on column sys_grupo_usuario.vc_usuarioc_rea is
'Identificador del usuario que crea el registro';

comment on column sys_grupo_usuario.dt_crea is
'Fecha-hora de creacion del registro';

comment on column sys_grupo_usuario.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column sys_grupo_usuario.dt_modifica is
'Fecha-hora de modificacion del registro';

/*==============================================================*/
/* Index: sys_grupo_usuario_pk                                  */
/*==============================================================*/
create unique index sys_grupo_usuario_pk on sys_grupo_usuario (
sys_vc_usuario,
sys_vc_grupo
);

/*==============================================================*/
/* Index: relationship_37_fk                                    */
/*==============================================================*/
create  index relationship_37_fk on sys_grupo_usuario (
sys_vc_usuario
);

/*==============================================================*/
/* Index: relationship_38_fk                                    */
/*==============================================================*/
create  index relationship_38_fk on sys_grupo_usuario (
sys_vc_grupo
);

/*==============================================================*/
/* Table: sys_pais                                              */
/*==============================================================*/
create table sys_pais (
   i_pais               SERIAL               not null,
   vc_pais              character varying(100) null,
   vc_codigo            character varying(3) null,
   b_eliminado          boolean              null,
   constraint PK_SYS_PAIS primary key (i_pais)
);

comment on table sys_pais is
'Entidad que registra los paises';

comment on column sys_pais.i_pais is
'Identificador unico o llave primaria del pais';

comment on column sys_pais.vc_pais is
'Nombre del pais';

comment on column sys_pais.vc_codigo is
'Codigo internacional asignado';

comment on column sys_pais.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: sys_pais_pk                                           */
/*==============================================================*/
create unique index sys_pais_pk on sys_pais (
i_pais
);

/*==============================================================*/
/* Table: sys_pais_division                                     */
/*==============================================================*/
create table sys_pais_division (
   i_pais_division      serial              not null,
   i_pais               INT4                 null,
   i_pais_zona          INT4                 null,
   vc_pais_division     character varying(100) null,
   b_eliminado          boolean              null,
   constraint PK_SYS_PAIS_DIVISION primary key (i_pais_division)
);

comment on table sys_pais_division is
'Entidad que registra la divisiÃ³n geografica del paÃ­s';

comment on column sys_pais_division.i_pais_division is
'Identificador unico o llave primaria de la division del pai­s';

comment on column sys_pais_division.i_pais is
'Identificador Ãºnico o llave primaria del pais';

comment on column sys_pais_division.i_pais_zona is
'Identificador Ãºnico o llave primaria de la zona';

comment on column sys_pais_division.vc_pais_division is
'Nombre del departamento o provincia';

comment on column sys_pais_division.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: sys_pais_division_pk                                  */
/*==============================================================*/
create unique index sys_pais_division_pk on sys_pais_division (
i_pais_division
);

/*==============================================================*/
/* Index: relationship_17_fk                                    */
/*==============================================================*/
create  index relationship_17_fk on sys_pais_division (
i_pais
);

/*==============================================================*/
/* Index: relationship_18_fk                                    */
/*==============================================================*/
create  index relationship_18_fk on sys_pais_division (
i_pais_zona
);

/*==============================================================*/
/* Table: sys_pais_zona                                         */
/*==============================================================*/
create table sys_pais_zona (
   i_pais_zona          serial                 not null,
   i_pais               INT4               null,
   vc_pais_zona         character varying(100) null,
   b_eliminado          boolean              null,
   constraint PK_SYS_PAIS_ZONA primary key (i_pais_zona)
);

comment on table sys_pais_zona is
'Entidad que registra las zonas en que se divide un paÃ­s';

comment on column sys_pais_zona.i_pais_zona is
'Identificador unico o llave primaria de la zona';

comment on column sys_pais_zona.i_pais is
'Identificador Ãºnico o llave primaria del pais';

comment on column sys_pais_zona.vc_pais_zona is
'Nombre de la zona';

comment on column sys_pais_zona.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: sys_pais_zona_pk                                      */
/*==============================================================*/
create unique index sys_pais_zona_pk on sys_pais_zona (
i_pais_zona
);

/*==============================================================*/
/* Table: sys_persona                                           */
/*==============================================================*/
create table sys_persona (
   i_persona            SERIAL               not null,
   i_estructura_org     INT4                 null,
   sys_i_persona        INT4                 null,
   i_institucion        smallint             not null,
   vc_cargo             character varying(50) null,
   vc_usuario_crea      character varying(150) null,
   vc_usuario_modif     character varying(150) null,
   vc_nombre            character varying(100) not null,
   vc_apellido          character varying(100) not null,
   vc_dui               character varying(30) not null,
   vc_correo            character varying(75) null,
   vc_institucion       character varying(80) null,
   vc_telefono          character varying(25) null,
   i_detalle            smallint             null,
   dt_crea              DATE                 not null,
   dt_modifica          DATE                 null,
   vc_profesion         character varying(100) null,
   b_activa             boolean              null,
   b_eliminado          boolean              null,
   dt_nacimiento        DATE                 null,
   vc_genero            character varying(1) null,
   constraint PK_SYS_PERSONA primary key (i_persona)
);

comment on table sys_persona is
'Entidad que registra los datos de la Persona';

comment on column sys_persona.i_persona is
'Identificador unico de la persona';

comment on column sys_persona.i_estructura_org is
'Identificador unico de la estructura organizativa';

comment on column sys_persona.sys_i_persona is
'Identificador unico de la persona';

comment on column sys_persona.i_institucion is
'Identificador unico o llave primaria para la institucion';

comment on column sys_persona.vc_cargo is
'Cargo desempeñado por la persona';

comment on column sys_persona.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column sys_persona.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column sys_persona.vc_nombre is
'Nombres de la persona';

comment on column sys_persona.vc_apellido is
'Apellidos de la persona';

comment on column sys_persona.vc_dui is
'Numero de documento unico de identidad de la persona';

comment on column sys_persona.vc_correo is
'Direccion de correo electronico';

comment on column sys_persona.vc_institucion is
'Nombre de la institucion';

comment on column sys_persona.vc_telefono is
'Numero telefenico';

comment on column sys_persona.i_detalle is
'Bandera de ultimo nivel';

comment on column sys_persona.dt_crea is
'Fecha-hora de creacion del registro';

comment on column sys_persona.dt_modifica is
'Fecha-hora de modificacion del registro';

comment on column sys_persona.vc_profesion is
'Profesion u oficio';

comment on column sys_persona.b_activa is
'Bandera que indica si el registro es activo o no. 1 Activo, 0 Inacativo';

comment on column sys_persona.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

comment on column sys_persona.dt_nacimiento is
'Fecha de nacimiento de la persona';

comment on column sys_persona.vc_genero is
'Genero a la que la persona pertenece solo se aceptara F o M';

/*==============================================================*/
/* Index: sys_persona_pk                                        */
/*==============================================================*/
create unique index sys_persona_pk on sys_persona (
i_persona
);

/*==============================================================*/
/* Index: relationship_31_fk                                    */
/*==============================================================*/
create  index relationship_31_fk on sys_persona (
i_estructura_org
);

/*==============================================================*/
/* Index: relationship_32_fk                                    */
/*==============================================================*/
create  index relationship_32_fk on sys_persona (
sys_i_persona
);

/*==============================================================*/
/* Table: sys_privilegio                                        */
/*==============================================================*/
create table sys_privilegio (
   i_privilegio         SERIAL               not null,
   b_activo             boolean              null,
   dt_crea              DATE                 null,
   vc_usuario_crea      character varying(150) null,
   vc_usuario_modif     character varying(150) null,
   dt_modifica          DATE                 null,
   vc_usuario           character varying(150) null,
   i_recurso            INT4                 null,
   b_pinsert            boolean              null,
   b_pselect            boolean              null,
   b_pupdate            boolean              null,
   b_pdelete            boolean              null,
   constraint PK_SYS_PRIVILEGIO primary key (i_privilegio)
);

comment on table sys_privilegio is
'Entidad que registra los privilegios asignados a los usuarios sobre cada recurso';

comment on column sys_privilegio.i_privilegio is
'Identificador de llave primaria de la tabla';

comment on column sys_privilegio.b_activo is
'Bandera que identifica si el usuario esta activo o no';

comment on column sys_privilegio.dt_crea is
'Fecha-hora de creacion del registro';

comment on column sys_privilegio.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column sys_privilegio.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column sys_privilegio.dt_modifica is
'Fecha-hora de modificacion del registro';

comment on column sys_privilegio.vc_usuario is
'Identificador unico del usuario del sistema';

comment on column sys_privilegio.i_recurso is
'Identificador unico del recurso u opcion del sistema';

comment on column sys_privilegio.b_pinsert is
'Determina si permite insertar';

comment on column sys_privilegio.b_pselect is
'Determina si permite ver';

comment on column sys_privilegio.b_pupdate is
'Determina si permite actualizar';

comment on column sys_privilegio.b_pdelete is
'Determina si permite eliminar';

/*==============================================================*/
/* Index: sys_privilegio_pk                                     */
/*==============================================================*/
create unique index sys_privilegio_pk on sys_privilegio (
i_privilegio
);

/*==============================================================*/
/* Index: relationship_40_fk                                    */
/*==============================================================*/
create  index relationship_40_fk on sys_privilegio (
vc_usuario
);

/*==============================================================*/
/* Index: relationship_41_fk                                    */
/*==============================================================*/
create  index relationship_41_fk on sys_privilegio (
i_recurso
);

/*==============================================================*/
/* Table: sys_usuario                                           */
/*==============================================================*/
create table sys_usuario (
   vc_usuario           character varying(150) not null,
   sys_vc_usuario       character varying(150) null,
   i_persona            INT4                 null,
   vc_clave             character varying(128) null,
   b_activo             boolean              not null,
   i_detalle            smallint             not null,
   vc_descripcion       character varying(254) null,
   i_camb_clave         smallint             not null,
   vc_usuario_crea      character varying(150) null,
   dt_crea              DATE                 null,
   vc_usuario_modif     character varying(150) null,
   dt_modifica          DATE                 null,
   b_eliminado          boolean              null,
   constraint PK_SYS_USUARIO primary key (vc_usuario)
);

comment on table sys_usuario is
'Entidad que registra los usuarios del sistema';

comment on column sys_usuario.vc_usuario is
'Identificador unico del usuario del sistema';

comment on column sys_usuario.sys_vc_usuario is
'Identificador unico del usuario del sistema';

comment on column sys_usuario.i_persona is
'Identificador Ãºnico de la Persona';

comment on column sys_usuario.vc_clave is
'Password o contraseña de acceso al sistema';

comment on column sys_usuario.b_activo is
'Bandera que identifica si el usuario esta activo o no';

comment on column sys_usuario.i_detalle is
'Bandera de ultimo bivel';

comment on column sys_usuario.vc_descripcion is
'Descripcion o especificacion';

comment on column sys_usuario.i_camb_clave is
'Bandera que identifica si el usuario cambio la clave de acceso al sistema';

comment on column sys_usuario.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column sys_usuario.dt_crea is
'Fecha-hora de creacion del registro';

comment on column sys_usuario.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column sys_usuario.dt_modifica is
'Fecha-hora de modificacion del registro';

comment on column sys_usuario.b_eliminado is
'Variable booleana que identifica si el registro es dado de baja';

/*==============================================================*/
/* Index: sys_usuario_pk                                        */
/*==============================================================*/
create unique index sys_usuario_pk on sys_usuario (
vc_usuario
);

/*==============================================================*/
/* Index: relationship_28_fk                                    */
/*==============================================================*/
create  index relationship_28_fk on sys_usuario (
i_persona
);

/*==============================================================*/
/* Index: relationship_36_fk                                    */
/*==============================================================*/
create  index relationship_36_fk on sys_usuario (
sys_vc_usuario
);

/*==============================================================*/
/* Table: sys_usuario_modulo                                    */
/*==============================================================*/
create table sys_usuario_modulo (
   vc_usuario           character varying(150) not null,
   vc_modulo            character varying(16) not null,
   vc_usuario_crea      character varying(150) null,
   dt_crea              DATE                 null,
   vc_usuario_modif     character varying(150) null,
   dt_modifica          DATE                 null,
   constraint PK_SYS_USUARIO_MODULO primary key (vc_usuario, vc_modulo)
);

comment on table sys_usuario_modulo is
'Entidad que registra los usuarios habilitados para cada mÃ³dulo o componente del sistema';

comment on column sys_usuario_modulo.vc_usuario is
'Identificador unico del usuario del sistema';

comment on column sys_usuario_modulo.vc_modulo is
'Identificador unico del modulo o componente del sistema';

comment on column sys_usuario_modulo.vc_usuario_crea is
'Identificador del usuario que crea el registro';

comment on column sys_usuario_modulo.dt_crea is
'Fecha-hora de creacion del registro';

comment on column sys_usuario_modulo.vc_usuario_modif is
'Identificador del usuario que modifica el registro';

comment on column sys_usuario_modulo.dt_modifica is
'Fecha-hora de modificacion del registro';

/*==============================================================*/
/* Index: sys_usuario_modulo_pk                                 */
/*==============================================================*/
create unique index sys_usuario_modulo_pk on sys_usuario_modulo (
vc_usuario,
vc_modulo
);

/*==============================================================*/
/* Index: relationship_26_fk                                    */
/*==============================================================*/
create  index relationship_26_fk on sys_usuario_modulo (
vc_usuario
);

/*==============================================================*/
/* Index: relationship_35_fk                                    */
/*==============================================================*/
create  index relationship_35_fk on sys_usuario_modulo (
vc_modulo
);

alter table cat_becario
   add constraint FK_CAT_BECA_FK_MONTO__CAT_MONT foreign key (i_monto)
      references cat_monto (i_monto)
      on delete restrict on update restrict;

alter table cat_becario
   add constraint FK_BECARIO_CARRERA foreign key (vc_cod_carr)
      references cat_carrera (vc_cod_carr)
      on delete restrict on update restrict;

alter table cat_becario
   add constraint FK_BECARIO_ESTUDIANTE foreign key (vc_carnet)
      references cat_estudiante (vc_carnet)
      on delete restrict on update restrict;

alter table cat_carrera
   add constraint FK_CARRERA_FACULTAD foreign key (vc_cod_fac)
      references cat_facultad (vc_cod_facultad)
      on delete restrict on update restrict;

alter table cat_correo
   add constraint FK_CORREO_ESTUDIANTE foreign key (vc_carnet)
      references cat_estudiante (vc_carnet)
      on delete restrict on update restrict;

alter table cat_domicilio
   add constraint FK_DOMICILIO_ESTUDIANTE foreign key (vc_carnet)
      references cat_estudiante (vc_carnet)
      on delete restrict on update restrict;

alter table cat_estudiante
   add constraint FK_ESTUDIANTE_CARRERA foreign key (vc_cod_carr)
      references cat_carrera (vc_cod_carr)
      on delete restrict on update restrict;

alter table cat_estudiante
   add constraint FK_ESTUDIANTE_FACULTAD foreign key (vc_cod_facultad)
      references cat_facultad (vc_cod_facultad)
      on delete restrict on update restrict;

alter table cat_institucion
   add constraint FK_INSTITUCION_PAIS foreign key (i_pais)
      references sys_pais (i_pais)
      on delete restrict on update restrict;

alter table cat_institucion
   add constraint FK_INSTITUCION_SERVIDOR foreign key (i_servidor)
      references cat_servidor (i_servidor)
      on delete restrict on update restrict;

alter table cat_recurso
   add constraint FK_RECURSO_RECURSO_PADRE foreign key (cat_i_recurso)
      references cat_recurso (i_recurso)
      on delete restrict on update restrict;

alter table cat_recurso
   add constraint FK_RECURSO_TIPO_RECURSO foreign key (vc_tipo_recurso)
      references cat_tipo_recurso (vc_tipo_recurso)
      on delete restrict on update restrict;

alter table cat_recurso
   add constraint FK_RECURSO_MODULO foreign key (vc_modulo)
      references cat_modulo (vc_modulo)
      on delete restrict on update restrict;

alter table cat_reg_materia
   add constraint FK_REG_MATERIA_ESTUDIANTE foreign key (vc_carnet)
      references cat_estudiante (vc_carnet)
      on delete restrict on update restrict;

alter table sys_cuf_acad
   add constraint FK_CUF_ESTUDIANTE foreign key (vc_carnet)
      references cat_estudiante (vc_carnet)
      on delete restrict on update restrict;

alter table sys_estructura_org
   add constraint FK_ESTRUCTURA_PADRE_ESTRUCTURA foreign key (sys_i_estructura_org)
      references sys_estructura_org (i_estructura_org)
      on delete restrict on update restrict;

alter table sys_estructura_org
   add constraint FK_ESTRUCTURA_INSTITUCION foreign key (i_institucion)
      references cat_institucion (i_institucion)
      on delete restrict on update restrict;

alter table sys_grupo_usuario
   add constraint FK_GRUPO_USUARIO foreign key (sys_vc_usuario)
      references sys_usuario (vc_usuario)
      on delete restrict on update restrict;

alter table sys_grupo_usuario
   add constraint FK_GRUPO_USUARIO_USUARIO foreign key (sys_vc_grupo)
      references sys_usuario (vc_usuario)
      on delete restrict on update restrict;

alter table sys_pais_division
   add constraint FK_PAIS_DIVISION_PAIS foreign key (i_pais)
      references sys_pais (i_pais)
      on delete restrict on update restrict;

alter table sys_pais_division
   add constraint FK_PAIS_DIVISION_PAIS_ZONA foreign key (i_pais_zona)
      references sys_pais_zona (i_pais_zona)
      on delete restrict on update restrict;

alter table sys_pais_zona
   add constraint FK_PAIS_ZONA_PAIS foreign key (i_pais)
      references sys_pais (i_pais)
      on delete restrict on update restrict;

alter table sys_persona
   add constraint FK_PERSONA_ESTRUCTURA foreign key (i_estructura_org)
      references sys_estructura_org (i_estructura_org)
      on delete restrict on update restrict;

alter table sys_persona
   add constraint FK_PERSONA_PERSONA_PADRE foreign key (sys_i_persona)
      references sys_persona (i_persona)
      on delete restrict on update restrict;

alter table sys_privilegio
   add constraint FK_PRIVILEGIO_USUARIO foreign key (vc_usuario)
      references sys_usuario (vc_usuario)
      on delete restrict on update restrict;

alter table sys_privilegio
   add constraint FK_SYS_PRIV_RELATIONS_CAT_RECU foreign key (i_recurso)
      references cat_recurso (i_recurso)
      on delete restrict on update restrict;

alter table sys_usuario
   add constraint FK_USUARIO_PERSONA foreign key (i_persona)
      references sys_persona (i_persona)
      on delete restrict on update restrict;

alter table sys_usuario
   add constraint FK_USUARIO_USUARIO_GRUPO foreign key (sys_vc_usuario)
      references sys_usuario (vc_usuario)
      on delete restrict on update restrict;

alter table sys_usuario_modulo
   add constraint FK_USUARIO_MODULO_GRUPO foreign key (vc_usuario)
      references sys_usuario (vc_usuario)
      on delete restrict on update restrict;

alter table sys_usuario_modulo
   add constraint FK_USUARIO_MODULO foreign key (vc_modulo)
      references cat_modulo (vc_modulo)
      on delete restrict on update restrict;

